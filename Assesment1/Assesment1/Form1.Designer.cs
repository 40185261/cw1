﻿namespace Assesment1
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.txtBoxname = new System.Windows.Forms.TextBox();
            this.txtBoxmatric = new System.Windows.Forms.TextBox();
            this.txtBoxlevel = new System.Windows.Forms.TextBox();
            this.txtBoxCredit = new System.Windows.Forms.TextBox();
            this.txtBoxCourse = new System.Windows.Forms.TextBox();
            this.txtBoxdob = new System.Windows.Forms.TextBox();
            this.txtBoxLast = new System.Windows.Forms.TextBox();
            this.buttonSet = new System.Windows.Forms.Button();
            this.buttonAdvance = new System.Windows.Forms.Button();
            this.buttonAward = new System.Windows.Forms.Button();
            this.buttonGet = new System.Windows.Forms.Button();
            this.buttonClear = new System.Windows.Forms.Button();
            this.Topic = new System.Windows.Forms.Label();
            this.Supervisor = new System.Windows.Forms.Label();
            this.splitter1 = new System.Windows.Forms.Splitter();
            this.txtBoxSupervisor = new System.Windows.Forms.TextBox();
            this.txtBoxTopic = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 33);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(35, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Name";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(1, 293);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(0, 13);
            this.label2.TabIndex = 1;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(13, 270);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(39, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "Credits";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(12, 220);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(33, 13);
            this.label4.TabIndex = 3;
            this.label4.Text = "Level";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(12, 126);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(40, 13);
            this.label5.TabIndex = 4;
            this.label5.Text = "Course";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(12, 79);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(66, 13);
            this.label6.TabIndex = 5;
            this.label6.Text = "Date of Birth";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(12, 176);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(53, 13);
            this.label7.TabIndex = 6;
            this.label7.Text = "Matric No";
            // 
            // txtBoxname
            // 
            this.txtBoxname.Location = new System.Drawing.Point(80, 26);
            this.txtBoxname.Name = "txtBoxname";
            this.txtBoxname.Size = new System.Drawing.Size(121, 20);
            this.txtBoxname.TabIndex = 7;
            this.txtBoxname.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
            // 
            // txtBoxmatric
            // 
            this.txtBoxmatric.Location = new System.Drawing.Point(80, 169);
            this.txtBoxmatric.Name = "txtBoxmatric";
            this.txtBoxmatric.Size = new System.Drawing.Size(121, 20);
            this.txtBoxmatric.TabIndex = 8;
            // 
            // txtBoxlevel
            // 
            this.txtBoxlevel.Location = new System.Drawing.Point(80, 217);
            this.txtBoxlevel.Name = "txtBoxlevel";
            this.txtBoxlevel.Size = new System.Drawing.Size(121, 20);
            this.txtBoxlevel.TabIndex = 9;
            // 
            // txtBoxCredit
            // 
            this.txtBoxCredit.Location = new System.Drawing.Point(80, 267);
            this.txtBoxCredit.Name = "txtBoxCredit";
            this.txtBoxCredit.Size = new System.Drawing.Size(121, 20);
            this.txtBoxCredit.TabIndex = 10;
            // 
            // txtBoxCourse
            // 
            this.txtBoxCourse.Location = new System.Drawing.Point(80, 123);
            this.txtBoxCourse.Name = "txtBoxCourse";
            this.txtBoxCourse.Size = new System.Drawing.Size(121, 20);
            this.txtBoxCourse.TabIndex = 11;
            // 
            // txtBoxdob
            // 
            this.txtBoxdob.Location = new System.Drawing.Point(80, 76);
            this.txtBoxdob.Name = "txtBoxdob";
            this.txtBoxdob.Size = new System.Drawing.Size(121, 20);
            this.txtBoxdob.TabIndex = 12;
            // 
            // txtBoxLast
            // 
            this.txtBoxLast.Location = new System.Drawing.Point(207, 26);
            this.txtBoxLast.Name = "txtBoxLast";
            this.txtBoxLast.Size = new System.Drawing.Size(121, 20);
            this.txtBoxLast.TabIndex = 13;
            // 
            // buttonSet
            // 
            this.buttonSet.Location = new System.Drawing.Point(15, 311);
            this.buttonSet.Name = "buttonSet";
            this.buttonSet.Size = new System.Drawing.Size(75, 23);
            this.buttonSet.TabIndex = 14;
            this.buttonSet.Text = "Set";
            this.buttonSet.UseVisualStyleBackColor = true;
            this.buttonSet.Click += new System.EventHandler(this.buttonSet_Click);
            // 
            // buttonAdvance
            // 
            this.buttonAdvance.Location = new System.Drawing.Point(207, 217);
            this.buttonAdvance.Name = "buttonAdvance";
            this.buttonAdvance.Size = new System.Drawing.Size(75, 23);
            this.buttonAdvance.TabIndex = 15;
            this.buttonAdvance.Text = "Advance";
            this.buttonAdvance.UseVisualStyleBackColor = true;
            this.buttonAdvance.Click += new System.EventHandler(this.buttonAdvance_Click);
            // 
            // buttonAward
            // 
            this.buttonAward.Location = new System.Drawing.Point(258, 311);
            this.buttonAward.Name = "buttonAward";
            this.buttonAward.Size = new System.Drawing.Size(75, 23);
            this.buttonAward.TabIndex = 16;
            this.buttonAward.Text = "Award";
            this.buttonAward.UseVisualStyleBackColor = true;
            this.buttonAward.Click += new System.EventHandler(this.buttonAward_Click);
            // 
            // buttonGet
            // 
            this.buttonGet.Location = new System.Drawing.Point(177, 311);
            this.buttonGet.Name = "buttonGet";
            this.buttonGet.Size = new System.Drawing.Size(75, 23);
            this.buttonGet.TabIndex = 17;
            this.buttonGet.Text = "Get";
            this.buttonGet.UseVisualStyleBackColor = true;
            this.buttonGet.Click += new System.EventHandler(this.buttonGet_Click);
            // 
            // buttonClear
            // 
            this.buttonClear.Location = new System.Drawing.Point(96, 311);
            this.buttonClear.Name = "buttonClear";
            this.buttonClear.Size = new System.Drawing.Size(75, 23);
            this.buttonClear.TabIndex = 18;
            this.buttonClear.Text = "Clear";
            this.buttonClear.UseVisualStyleBackColor = true;
            this.buttonClear.Click += new System.EventHandler(this.buttonClear_Click);
            // 
            // Topic
            // 
            this.Topic.AutoSize = true;
            this.Topic.Location = new System.Drawing.Point(10, 370);
            this.Topic.Name = "Topic";
            this.Topic.Size = new System.Drawing.Size(34, 13);
            this.Topic.TabIndex = 19;
            this.Topic.Text = "Topic";
            this.Topic.Click += new System.EventHandler(this.label8_Click);
            // 
            // Supervisor
            // 
            this.Supervisor.AutoSize = true;
            this.Supervisor.Location = new System.Drawing.Point(8, 409);
            this.Supervisor.Name = "Supervisor";
            this.Supervisor.Size = new System.Drawing.Size(57, 13);
            this.Supervisor.TabIndex = 20;
            this.Supervisor.Text = "Supervisor";
            // 
            // splitter1
            // 
            this.splitter1.Location = new System.Drawing.Point(0, 0);
            this.splitter1.Name = "splitter1";
            this.splitter1.Size = new System.Drawing.Size(3, 431);
            this.splitter1.TabIndex = 21;
            this.splitter1.TabStop = false;
            // 
            // txtBoxSupervisor
            // 
            this.txtBoxSupervisor.Location = new System.Drawing.Point(80, 402);
            this.txtBoxSupervisor.Name = "txtBoxSupervisor";
            this.txtBoxSupervisor.Size = new System.Drawing.Size(121, 20);
            this.txtBoxSupervisor.TabIndex = 22;
            // 
            // txtBoxTopic
            // 
            this.txtBoxTopic.Location = new System.Drawing.Point(80, 363);
            this.txtBoxTopic.Name = "txtBoxTopic";
            this.txtBoxTopic.Size = new System.Drawing.Size(121, 20);
            this.txtBoxTopic.TabIndex = 23;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(358, 431);
            this.Controls.Add(this.txtBoxTopic);
            this.Controls.Add(this.txtBoxSupervisor);
            this.Controls.Add(this.splitter1);
            this.Controls.Add(this.Supervisor);
            this.Controls.Add(this.Topic);
            this.Controls.Add(this.buttonClear);
            this.Controls.Add(this.buttonGet);
            this.Controls.Add(this.buttonAward);
            this.Controls.Add(this.buttonAdvance);
            this.Controls.Add(this.buttonSet);
            this.Controls.Add(this.txtBoxLast);
            this.Controls.Add(this.txtBoxdob);
            this.Controls.Add(this.txtBoxCourse);
            this.Controls.Add(this.txtBoxCredit);
            this.Controls.Add(this.txtBoxlevel);
            this.Controls.Add(this.txtBoxmatric);
            this.Controls.Add(this.txtBoxname);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txtBoxname;
        private System.Windows.Forms.TextBox txtBoxmatric;
        private System.Windows.Forms.TextBox txtBoxlevel;
        private System.Windows.Forms.TextBox txtBoxCredit;
        private System.Windows.Forms.TextBox txtBoxCourse;
        private System.Windows.Forms.TextBox txtBoxdob;
        private System.Windows.Forms.TextBox txtBoxLast;
        private System.Windows.Forms.Button buttonSet;
        private System.Windows.Forms.Button buttonAdvance;
        private System.Windows.Forms.Button buttonAward;
        private System.Windows.Forms.Button buttonGet;
        private System.Windows.Forms.Button buttonClear;
        private System.Windows.Forms.Label Topic;
        private System.Windows.Forms.Label Supervisor;
        private System.Windows.Forms.Splitter splitter1;
        private System.Windows.Forms.TextBox txtBoxSupervisor;
        private System.Windows.Forms.TextBox txtBoxTopic;
    }
}

