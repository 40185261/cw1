﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assesment1

/**
* Store details of a student.
* 
* @author Ayok Simon
* @version October 2015
*/
{
    class student
    {
        /*The class shows name of student, matric number, date of birth course, level and credits*/
        private int Matric = 40000;
        private String Name;
        private String last;
        private String dob;
        private String Course;
        private int Level = 1;
        private int Credits = 0;
       





        /**
        @Set and return matriculation number in which the student was registered.
          value in the range 40000 ... 60000
       */
        public int MatricNO
        {
            get
            {
                return Matric;
            }
            set
            {
                if (value > 60000)
                {
                    throw new ArgumentException("Not valid Marticulation Number");
                }
                Matric = value;
            }
        }
        /**
        @Set and return first name of student registered.
        */
        public String name
        {
            get
            {
                return Name;
            }
            set
            {
                Name = value;
            }
        }
        /**
        @Set and return Last name of student registered.
        */
        public String LastName
        {
            get
            {
                return last;
            }
            set
            {
                last = value;
            }
        }
        /**
        @Set and return date of birth of student registered.
        */
        public String DateofBirth
        {
            get
            {
                return dob;
            }
            set
            {
                dob = value;
            }
        }
        /**
        @Set and return course of student registered.
        */
        public String course
        {
            get
            {
                return Course;
            }
            set
            {
                Course = value;
            }

        }
        /**
        @Set and return level of student registered.
        Range from 1 ... 4
            */
        public int level
        {
            get
            {
                return Level;
            }
            set
            {
                if (value > 4)
                {
                    throw new ArgumentException("Not valid level, please try again");
                }
                Level = value;
            }

        }
        /**
        @Set and return credits of student registered.
        */
        public int credit
        {
            get
            {
                return Credits;
            }
            set
            {
                if (value > 480)
                {
                    throw new ArgumentException("Not valid credits, please try again");
                }
                Credits = value;
            }
        }
        /**
        A method to advance a student to next level following some certin criterial .
        */
        public String advance()
        {
            if (Level == 4)
            {
                return "Student is already in 4th year";
            }
            else if (Credits >= 360)
            {
                Level = 4;
                return "Student advanced to level 4";
            }
            else if (Credits >= 240)
            {
                Level = 3;
                return "Student advanced to level 3";
            }
            else if (Credits >= 120)
            {
                Level = 2;
                return "Student advanced to level 2";
            }
            else
            {
                Level = 1;
                return "Student do not have enough credits";
            }

        }
        /**
          A method to award a student following some certin criterial .
        */
        public String award()
        {
            if (Course == "PHD")
            {
                return "Doctor of Philosophy ";
            }
            if (Credits < 360)
            {
                return "Certificate of Higher Education";
            }
            else if (Credits < 480)
            {
                return "Degree";
            }
             else
            {
                return "Honours degree";

            }
                
            }

        
    }
}
        

    


