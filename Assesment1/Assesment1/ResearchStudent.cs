﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assesment1
/**
* Store details of a Research student.
* 
* @author Ayok Simon
* @version October 2015
*/
{
    // inheriting properties from student class
    class ResearchStudent : student 
    {
        private String Topic;
        private String Supervisor;
        

        // @Set and return Topic of Research student .

        public String topic
        {
            get
            {
                return Topic;
            }
            set
            {
                Topic = value;
            }
        }
        /**
        @Set and return supervisorstudent registered.
                   */
        public String supervisor
        {
            get
            {
                return Supervisor;
            }
            set
            {
                Supervisor = value;
            }
        }

    }
}
