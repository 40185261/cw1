﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Assesment1
 /* Store detalis of students .
  @authur Ayok Simon
  ID 40185261
  @version october 2015*/
{
    public partial class Form1 : Form
    {
        private student StudentForm = new student();
        private ResearchStudent ResearchStudentForm = new ResearchStudent();
        public Form1()
        {
            InitializeComponent();
        }
        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }
        // Getting student info which has been set 
        private void buttonGet_Click(object sender, EventArgs e)
        {
            txtBoxname.Text = StudentForm.name;
            txtBoxLast.Text = StudentForm.LastName;
            txtBoxdob.Text = StudentForm.DateofBirth;
            txtBoxCourse.Text = StudentForm.course;
            txtBoxmatric.Text = Convert.ToString(StudentForm.MatricNO);
            txtBoxlevel.Text = Convert.ToString(StudentForm.level);
            txtBoxCredit.Text = Convert.ToString(StudentForm.credit);
            txtBoxTopic.Text = ResearchStudentForm.topic;
            txtBoxSupervisor.Text = ResearchStudentForm.supervisor;
        }
        // Reading and setting student info from text box
        private void buttonSet_Click(object sender, EventArgs e)
        {
            StudentForm.name = txtBoxname.Text;
            StudentForm.LastName = txtBoxLast.Text;
            StudentForm.DateofBirth = txtBoxdob.Text;
            StudentForm.course = txtBoxCourse.Text;
            StudentForm.MatricNO = Convert.ToInt32(txtBoxmatric.Text);
            StudentForm.level = Convert.ToInt32(txtBoxlevel.Text);
            StudentForm.credit = Convert.ToInt32(txtBoxCredit.Text);
            ResearchStudentForm.topic = txtBoxTopic.Text;
            ResearchStudentForm.supervisor = txtBoxSupervisor.Text;
        }
        // Clearing student info from text box
        private void buttonClear_Click(object sender, EventArgs e)
        {
            txtBoxname.Clear();
            txtBoxLast.Clear();
            txtBoxdob.Clear();
            txtBoxCourse.Clear();
            txtBoxmatric.Clear();
            txtBoxlevel.Clear();
            txtBoxCredit.Clear();
            txtBoxSupervisor.Clear();
            txtBoxTopic.Clear();
        }
        // Calling the advance method and printing info on screen 
        private void buttonAdvance_Click(object sender, EventArgs e)
        {
            MessageBox.Show(StudentForm.advance());
        }
        // printing award message on screen 
        private void buttonAward_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Student No: " + StudentForm.MatricNO + "  Name: " + StudentForm.LastName + " " +
            StudentForm.name + " " + " Course :" + StudentForm.course + " " + "Level:" + " "+
            StudentForm.level  + " Award " + StudentForm.award());

        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void label8_Click(object sender, EventArgs e)
        {

        }

    }
}
